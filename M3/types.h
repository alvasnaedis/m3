#pragma once

#include <cstdint>
#include <iostream>

namespace emu
{

using namespace std;

typedef uint8_t byte;
typedef uint16_t word;

enum flag : byte
{
    CF = 0b00000001, // The carry flag, set if there was a carry after the most significant bit.
    NF = 0b00000010, // Shows whether the last operation was an addition (0) or an subtraction (1).
    PF = 0b00000100, // This flag can either be the parity of the result (PF), or the 2-compliment signed overflow (VF): set if 2-complement value doesn't fit in the register.
    XF = 0b00001000, // A copy of bit 3 of the result.
    HF = 0b00010000, // The half-carry of an addition/subtraction (from bit 3 to 4).
    YF = 0b00100000, // A copy of bit 5 of the result.
    ZF = 0b01000000, // Set if the result is zero.
    SF = 0b10000000, // Set if the 2-complement value is negative. A copy of the most significant bit.
};

template<typename T>
struct reg
{
    T val;

    void ld(const T v) { val = v; }
    void add(const T v) { val += v; }
    void sub(const T v) { val -= v; }
    void inc() { ++val; }
    void dec() { --val; }
    void ex(reg<T> &r) { swap(val, r.val); }

    operator T() { return val; }
};

typedef reg<byte> reg8;
typedef reg<word> reg16;

} // namespace emu
