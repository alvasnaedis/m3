#pragma once

#include "opcodes.h"
#include "types.h"
#include <array>
#include <bitset>
#include <iomanip>
#include <stack>

// Registers that can be used both as 8x2 and 16x1.
#if __BIG_ENDIAN__

#define REG(A, B) union { struct { reg8 A, B; }; reg16 A##B; };
#define REGa(A, B) union { struct { reg8 A##a, B##a; }; reg16 A##Ba; };

#elif __LITTLE_ENDIAN__

#define REG(A, B) union { struct { reg8 B, A; }; reg16 A##B; };
#define REGa(A, B) union { struct { reg8 B##a, A##a; }; reg16 A##Ba; };

#else

#error implement REG[a] for this endianness

#endif

namespace emu
{

using namespace std;

const auto RAM_AMOUNT = 0x2000;

struct z80
{
    // Accumulator and flags.
    REG(A, F);

    // General purpose registers.
    REG(B, C);
    REG(D, E);
    REG(H, L);

    // Index registers.
    reg16 IX, IY;

    // Program counter & stack pointer.
    reg16 PC, SP;

    // Interrupt vector base register & DRAM refresh counter.
    reg8 I, R;

    // Alternate registers.
    REGa(A, F);
    REGa(B, C);
    REGa(D, E);
    REGa(H, L);

    array<byte, RAM_AMOUNT> ram;
    stack<byte> stack;

    bool execute(const opcode op)
    {
        R.inc();
        PC.inc();

        if (PC == 0x00) {
#if !NDEBUG
            cout << "pc overflow" << endl;
#endif
            return false;
        }

        switch (op) {
        case NOP:
            break;

        // 8-bit load group.
        case LD_A_A: A.ld(A); break;
        case LD_A_B: A.ld(B); break;
        case LD_A_C: A.ld(C); break;
        case LD_A_D: A.ld(D); break;
        case LD_A_E: A.ld(E); break;
        case LD_A_H: A.ld(H); break;
        case LD_A_L: A.ld(L); break;
        case LD_A_HLi: A.ld(ram[HL]); break;
        case LD_A_BCi: A.ld(ram[BC]); break;
        case LD_A_DEi: A.ld(ram[DE]); break;

        case LD_B_A: B.ld(A); break;
        case LD_B_B: B.ld(B); break;
        case LD_B_C: B.ld(C); break;
        case LD_B_D: B.ld(D); break;
        case LD_B_E: B.ld(E); break;
        case LD_B_H: B.ld(H); break;
        case LD_B_L: B.ld(L); break;
        case LD_B_HLi: B.ld(ram[HL]); break;

        case LD_C_A: C.ld(A); break;
        case LD_C_B: C.ld(B); break;
        case LD_C_C: C.ld(C); break;
        case LD_C_D: C.ld(D); break;
        case LD_C_E: C.ld(E); break;
        case LD_C_H: C.ld(H); break;
        case LD_C_L: C.ld(L); break;
        case LD_C_HLi: C.ld(ram[HL]); break;

        case LD_HLi_A: ram[HL] = A; break;
        case LD_HLi_B: ram[HL] = B; break;
        case LD_HLi_C: ram[HL] = C; break;
        case LD_HLi_D: ram[HL] = D; break;
        case LD_HLi_E: ram[HL] = E; break;
        case LD_HLi_F: ram[HL] = F; break;
        case LD_HLi_L: ram[HL] = L; break;
        case LD_BCi_A: ram[BC] = A; break;
        case LD_DEi_A: ram[DE] = A; break;

        // 16-bit load group.
        case LD_SP_HL: SP.ld(HL); break;
        case LD_SPi_AF: ram[SP] = A; ram[SP + 1] = F; break;
        case LD_SPi_BC: ram[SP] = B; ram[SP + 1] = C; break;
        case LD_SPi_DE: ram[SP] = D; ram[SP + 1] = E; break;
        case LD_SPi_HL: ram[SP] = H; ram[SP + 1] = L; break;

        // Exchange, block transfer, search group.

        // 8-bit arithmetic and logical group.

        // General-purpose arithmetic and CPU control group.

        // 16-bit arithmetic group.

        // Rotate and shift group.

        // Bit set, reset and test group.

        // Jump group.

        // Call and return group.

        // Input and output group.

        // Prefixes.
        case CB_prefix:
            cout << "cb prefix:" << hex << setfill('0') << setw(2) << op << endl;
            break;
        case ED_prefix:
            cout << "ed prefix:" << hex << setfill('0') << setw(2) << op << endl;
            break;
        case DD_prefix:
            cout << "dd prefix:" << hex << setfill('0') << setw(2) << op << endl;
            break;
        case FD_prefix:
            cout << "fd prefix:" << hex << setfill('0') << setw(2) << op << endl;
            break;

        default:
#if !NDEBUG
            cout << "unimplemented op: " << hex << setfill('0') << setw(2) << op << endl;
#endif
            break;
        }

        return true;
    }

    void set_flag(const flag f)
    {
        F.val |= f;
    }

    void unset_flag(const flag f)
    {
        F.val &= ~f;
    }

    bool test_flag(const flag f) const
    {
        return (F.val & f) == f;
    }

    void print()
    {
        #define PR(reg, n) cout << #reg"=" << setw(n) << hex << static_cast<word>(reg)
        #define PR8(reg) PR(reg, 2)
        #define PR16(reg) PR(reg, 4)
        #define PB(reg, n) cout << #reg"=" << bitset<n>(reg)
        #define PB8(reg) PB(reg, 8)
        #define S << ' '
        #define BR << endl
        cout << setfill('0');
        PR8(A) S; PB8(F) BR;
        PR8(B) S; PR8(C) BR;
        PR8(D) S; PR8(E) BR;
        PR8(H) S; PR8(L) BR;
        PR16(IX) BR;
        PR16(IY) BR;
        PR16(PC) BR;
        PR16(SP) BR;
        PR8(I) S; PR8(R) BR;
    }
};

} // namespace emu
