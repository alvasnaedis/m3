#pragma once

#include "types.h"

#define $(op, id) id = 0x##op

namespace emu
{

// i = indexed
// s = shadow reg
enum opcode : byte
{
    $(00, NOP),

    // Prefixes.
    $(CB, CB_prefix),
    $(ED, ED_prefix),
    $(DD, DD_prefix), // Also DD CB.
    $(FD, FD_prefix), // Also FD CB.

    // 8-bit load group.
    $(7F, LD_A_A),
    $(78, LD_A_B),
    $(79, LD_A_C),
    $(7A, LD_A_D),
    $(7B, LD_A_E),
    $(7C, LD_A_H),
    $(7D, LD_A_L),
    $(7E, LD_A_HLi),
    $(0A, LD_A_BCi),
    $(1A, LD_A_DEi),

    $(47, LD_B_A),
    $(40, LD_B_B),
    $(41, LD_B_C),
    $(42, LD_B_D),
    $(43, LD_B_E),
    $(44, LD_B_H),
    $(45, LD_B_L),
    $(46, LD_B_HLi),

    $(4F, LD_C_A),
    $(48, LD_C_B),
    $(49, LD_C_C),
    $(4A, LD_C_D),
    $(4B, LD_C_E),
    $(4C, LD_C_H),
    $(4D, LD_C_L),
    $(4E, LD_C_HLi),

    $(77, LD_HLi_A),
    $(70, LD_HLi_B),
    $(71, LD_HLi_C),
    $(72, LD_HLi_D),
    $(73, LD_HLi_E),
    $(74, LD_HLi_F),
    $(75, LD_HLi_L),
    $(02, LD_BCi_A),
    $(12, LD_DEi_A),

    // 16-bit load group.
    $(F9, LD_SP_HL),
    $(F6, LD_SPi_AF),
    $(C6, LD_SPi_BC),
    $(D6, LD_SPi_DE),
    $(E6, LD_SPi_HL),

    // Exchange, block transfer, search group.

    // 8-bit arithmetic and logical group.

    // General-purpose arithmetic and CPU control group.

    // 16-bit arithmetic group.

    // Rotate and shift group.

    // Bit set, reset and test group.

    // Jump group.

    // Call and return group.

    // Input and output group.

};

} // namespace emu
