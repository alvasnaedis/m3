#pragma once

#include "psg.h"
#include "vdp.h"
#include "z80.h"
#include <fstream>

namespace emu
{

struct m3
{
    z80 cpu;
    vdp gpu;
    psg apu;
//    fm ym;

    vector<byte> rom;

    bool load(string &path)
    {
        ifstream is(path);
        if (!is) { return false; }
        istream_iterator<byte> start(is), end;
        vector<byte> data(start, end);
        rom.swap(data);
#if !NDEBUG
        cout << "loaded " << rom.size() << " bytes into ROM" << endl;
#endif
        return true;
    }

    int run()
    {
        if (!rom.size()) { return 1; }
        while (cpu.execute(static_cast<opcode>(rom[cpu.PC])));
        cpu.print();
        return 0;
    }
};

} // namespace emu
