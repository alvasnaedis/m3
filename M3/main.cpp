#include "emu.h"

#define ROM_PATH "/Users/jonas/Downloads/Sonic the Hedgehog 2/Sonic the Hedgehog 2.sms"

using namespace emu;

int main(int argc, const char *argv[])
{
    m3 emu;
    string rom = argc > 1 ? argv[1] : ROM_PATH;
    emu.load(rom);
    return emu.run();
}
