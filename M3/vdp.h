#pragma once

#include "z80.h"
#include <array>
#include <vector>

namespace emu
{

using namespace std;

const auto VRAM_AMOUNT = 0x4000;

struct vdp
{
    array<byte, VRAM_AMOUNT> vram;
    array<byte, 0xF000> fbuf; // Max 256x240.
};

} // namespace emu
